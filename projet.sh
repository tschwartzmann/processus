#! /bin/bash

#Thierry SCHWARTZMANN

###########################################################################################################
################################## Calcul de la charge pour un processus ##################################
###########################################################################################################

#fonction qui renvoie le nom du PID
function NomduPid() {
nom=$(cat /proc/$pid/stat | cut -d " " -f 2)
var=${nom#"("}
nom=$(echo ${var%")"})
}

#fonction qui renvoie la charge CPU avec son PID
function chargeCPUAvecPid() {
NomduPid
tempsecoule=0 #temps au bout duquel la charge CPU dépasse le seuil
tempsecoulePause=0 #temps de pause
seuil=40 #seuil de 40% pour la charge CPU
NORMAL="\\033[0m" #couleur normale
RVIG="\\033[31;7;1m" #couleur rouge, vidéo inversée, en gras

echo -e "$NORMAL""name\t\tpid\t\tchargeCPU"

#boucle infinie pour calculer la charge d'un processus
while true;
do
	#calcul de ttime à l'instant t1
	read val1 val2 val3 val4 val5 < <(cat /proc/stat | grep ^"cpu ")
	ttime1=$(( $val2+$val3+$val4 ))

	#calcul de utime à l'instant t1
	utime1=$(cat /proc/$pid/stat | cut -d " " -f 14)

	#pause de 2 secondes entre les deux instants
	sleep 2

	#calcul de ttime à l'instant t2
	read val1 val2 val3 val4 val5 < <(cat /proc/stat | grep ^"cpu ")
	ttime2=$(( $val2+$val3+$val4 ))

	#calcul de utime à l'instant t2
	utime2=$(cat /proc/$pid/stat | cut -d " " -f 14)

	#calcul de la difference entre utime1 et utime2
	utime=$(( $utime2 - $utime1 ));

	#calcul de la difference entre ttime1 et ttime2
	ttime=$(( $ttime2 - $ttime1 ));
	
	#on se place dans le cas où la différence entre ttime1 et ttime2 est différent de 0
	if [ $ttime != 0 ];then

		#division de utime et ttime
		div=$(echo "scale=2; $utime/$ttime;" | bc)
	
		#calcul de la charge CPU pour un processus
		chargeCPU=$(echo " $div * 100 " | bc);
		########################################################

		#test quand la charge CPU est supérieure au seuil
		test=$(echo  $chargeCPU'>'$seuil | bc -l )
				
		if [ $chargeCPU != 0 ];then
			if [ $test = 1 ];then
				tempsecoule=$(( $tempsecoule+1 ))
				if [ $tempsecoule = 5 ];then
					printf '\n'
					echo -e "$NORMAL""Pause[p]\tContinuer[c]\tArreter[a]"
					read reponse
					if [ $reponse = "p" ];then
						kill -SIGSTOP $pid
						echo -e "$NORMAL""reprendre le processus ? o/n"
						read reponse
						if [ $reponse = "o" ];then
							kill -SIGCONT $pid
						elif [ $reponse = "n" ];then
							echo -e "$NORMAL""le processus restera en pause, le programme se quitte"
							exit 0
						else
							echo -e "$NORMAL""le processus va etre fermé par le programme, le programme se quitte"
							kill -KILL $pid
							exit 0
						fi
					elif [ $reponse = "a" ];then
						echo -e "$NORMAL""le processus va etre fermé par le programme, le programme se quitte"
						kill -KILL $pid
						exit 0
					else
						tempsecoule=0
					fi
				
				fi
			
			else
				tempsecoule=0
			fi
		else
			tempsecoulePause=$(( $tempsecoulePause+1 ))
			if [ $tempsecoulePause = 5 ];then
				echo -e "$NORMAL""Le processus est peut-etre en pause. Voulez-vous le reprendre ? o/n"
				read reponse
				if [ $reponse = "o" ];then
					kill -SIGCONT $pid
				elif [ $reponse = "n" ];then
					echo -e "$NORMAL""le processus restera en pause, le programme se quitte"
					kill -SIGSTOP $pid
					exit 0
				else
					echo -e "$NORMAL""le processus va etre fermé par le programme, le programme se quitte"
					kill -KILL $pid
					exit 0
				fi
			fi
		fi
		
		########################################################################################

		#affichage du nom, du PID et de la charge CPU pour un processus avec option de la couleur
		if [ $test = 1 ];then
			#si la taille du nom est inférieure ou égale à 7
			if [ ${#nom} -le 7 ];then
				echo -n -e "$RVIG""$nom\t\t$pid\t\t$chargeCPU%\r"
			else
				echo -n -e "$RVIG""$nom\t$pid\t\t$chargeCPU%\r"
			fi
		else
			if [ ${#nom} -le 7 ];then
				echo -n -e "$NORMAL""$nom\t\t$pid\t\t$chargeCPU%\r"
			else
				echo -n -e "$NORMAL""$nom\t$pid\t\t$chargeCPU%\r"
			fi
		fi
	fi

#fin de la boucle
done
}


#################################################################################################################
################################## Calcul de la charge pour tous les processus ##################################
#################################################################################################################

#teste l'existence des fichiers
if [ -f "calcul.txt" ];then
	rm calcul.txt
fi

if [ -f "pid.txt" ];then
	rm pid.txt
fi

if [ -f "charges.txt" ];then
	rm charges.txt
fi

#fonction qui calcule la charge pour tous les processus
function ChargesTousProc() {

NORMAL="\\033[0m" #couleur normale

#liste tous les PID dans un fichier pid.txt
ls /proc | grep ^[0-9]*[0-9] >> pid.txt

#calcul de ttime1
read val1 val2 val3 val4 val5 < <(cat /proc/stat | grep ^"cpu ")
ttime1=$(( $val2+$val3+$val4 ))

#parcours du fichier pid.txt
for pid in $(cat pid.txt);do

	#teste si le dossier du PID existe
	if [ -d "/proc/$pid/" ];then

		#calcul de utime1
		utime1=$(cat /proc/$pid/stat | cut -d " " -f 14)

		NomduPid

		#sauvegarde dans un fichier texte (calcul.txt) le nom, le PID, le utime1 et le ttime1 de chaque processus
		echo "$nom\t\t$pid\t\t$utime1\t\t$ttime1" >> calcul.txt
	fi
done

#pause de 2 secondes
sleep 2

#calcul de ttime2
read val1 val2 val3 val4 val5 < <(cat /proc/stat | grep ^"cpu ")
ttime2=$(( $val2+$val3+$val4 ))

#parcours du fichier calcul.txt
for ligne in $(cat calcul.txt);do

	#lecture des lignes de nom, PID, utime1 et ttime1
	read nom pid utime1 ttime1 reste < <(echo -e $ligne)

	#teste si le dossier du PID existe
	if [ -d "/proc/$pid/" ];then
	
		#calcul de utime2    	
		utime2=$(cat /proc/$pid/stat | cut -d " " -f 14)

		#calcul de la différence entre utime1 et utime2
    		utime=$(( $utime2 - $utime1 ));

		#calcul de la difference entre ttime1 et ttime2
		ttime=$(( $ttime2 - $ttime1 ));

		#on se place dans le cas où la différence entre ttime1 et ttime2 est différente de 0			
		if [ $ttime != 0 ];then

			#division de utime et ttime
			div=$(echo "scale=2; $utime/$ttime;" | bc)

			#on se place dans le cas où la division entre utime et ttime est différente de 0			
			if [ $div != 0 ];then

				#calcul de la charge CPU pour tous les processus
				chargeCPU=$(echo " $div * 100 " | bc);

				if [ ${#nom} -le 7 ];then
					
					#renvoie le nom, le PID et la charge dans un fichier charges.txt
					echo "$nom\t\t$pid\t\t$chargeCPU%" >> charges.txt
				else
					echo "$nom\t$pid\t\t$chargeCPU%" >> charges.txt
				fi
			fi
		fi
	fi
done

#affichage des colonnes name, pid et chargeCPU
echo -e "$NORMAL""name\t\tpid\t\tchargeCPU"

#tri par ordre décroissant de la charge CPU lors du parcours du fichier charges.txt
charges=$(cat charges.txt | sort -t\t -k5nr)
for charge in $charges;do
	echo -e $charge
done

#choix du processus à suivre
echo -e "$NORMAL""Quel processus voulez-vous suivre ?"
read choix
pid=$(pidof $choix)
if [ ${#pid} != 0 ] && [ -d "/proc/$pid/" ];then
	chargeCPUAvecPid
else
	echo -e "$NORMAL""Processus invalide, veuillez relancer le script"
	exit 0
fi
}


##################################################################################################################
############################################# options avec arguments #############################################
##################################################################################################################

#charge de tous les processus soit sans argument, soit avec uniquement l'argument -p, soit avec uniquement l'argument -n
if ([ $# = 0 ] || ([ $1 = "-p" ] && [ -z $2 ]) || ([ $1 = "-n" ] && [ -z $2 ]));then
	ChargesTousProc
fi

#charge d'un processus avec PID avec l'argument -p et le numéro du PID
if ([ $1 = "-p" ] && [ -n $2 ]);then
	pid=$2
	if [ ${#pid} != 0 ] && [ -d "/proc/$pid/" ];then
		chargeCPUAvecPid
	else
		echo -e "$NORMAL""Le numéro du processus n'existe pas"
		ChargesTousProc
	fi
fi

#charge d'un processus avec PID avec l'argument -n et le nom du PID
if ([ $1 = "-n" ] && [ -n $2 ]);then
	pid=$(pidof $2)
	if [ ${#pid} != 0 ] && [ -d "/proc/$pid/" ];then
		chargeCPUAvecPid
	else
		echo -e "$NORMAL""Le nom du processus n'existe pas"
		ChargesTousProc
	fi
fi

